'use strict';

const Controller = require('egg').Controller;

class ProductController extends Controller {
  async index() {
    const { ctx } = this;
    const res = await ctx.service.product.index();
    ctx.body = res;
  }

  async detail() {
    const { ctx } = this;
    ctx.body = `id=${ctx.query.id}`;
  }

  async detail2() {
    const { ctx } = this;
    ctx.body = `pid=${ctx.params.id}`;
  }  

  async create() {
    const { ctx } = this;
    console.log(ctx.request.body);
    const {name, age} = ctx.request.body;
    ctx.body = {
      name,
      age
    }
  }

  async update() {
    const { ctx } = this;
    console.log(ctx.params);
    const {id} = ctx.params;
    ctx.body = {
      id
    }
  }

  async delete() {
    const { ctx } = this;
    console.log(ctx.params);
    const {id} = ctx.params;
    ctx.body = {
      id
    }
  }

}

module.exports = ProductController;
